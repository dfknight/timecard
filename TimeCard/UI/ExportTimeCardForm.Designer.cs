﻿namespace TimeCard.UI
{
    partial class ExportTimeCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportTimeCardForm));
            this.locationBox = new System.Windows.Forms.TextBox();
            this.infoLabel = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.exportTypeBox = new System.Windows.Forms.ComboBox();
            this.exportTypeLabel = new System.Windows.Forms.Label();
            this.exportFromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.exportButton = new System.Windows.Forms.Button();
            this.fromLabel = new System.Windows.Forms.Label();
            this.toLabel = new System.Windows.Forms.Label();
            this.exportToDatePicker = new System.Windows.Forms.DateTimePicker();
            this.clientNameLabel = new System.Windows.Forms.Label();
            this.clientNameBox = new System.Windows.Forms.ComboBox();
            this.matterNameLabel = new System.Windows.Forms.Label();
            this.matterNameBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // locationBox
            // 
            this.locationBox.Location = new System.Drawing.Point(18, 58);
            this.locationBox.Name = "locationBox";
            this.locationBox.Size = new System.Drawing.Size(302, 20);
            this.locationBox.TabIndex = 0;
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel.Location = new System.Drawing.Point(13, 13);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(259, 25);
            this.infoLabel.TabIndex = 1;
            this.infoLabel.Text = "Set Parameters for Export";
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(326, 56);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 2;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // exportTypeBox
            // 
            this.exportTypeBox.FormattingEnabled = true;
            this.exportTypeBox.Items.AddRange(new object[] {
            "Export All Data",
            "Export Data for this week (Monday to Sunday)",
            "Export Date Range (Based on Start Date)",
            "Export Client",
            "Export Matter"});
            this.exportTypeBox.Location = new System.Drawing.Point(455, 54);
            this.exportTypeBox.Name = "exportTypeBox";
            this.exportTypeBox.Size = new System.Drawing.Size(320, 21);
            this.exportTypeBox.TabIndex = 3;
            this.exportTypeBox.SelectedIndexChanged += new System.EventHandler(this.exportTypeBox_SelectedIndexChanged);
            // 
            // exportTypeLabel
            // 
            this.exportTypeLabel.AutoSize = true;
            this.exportTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportTypeLabel.Location = new System.Drawing.Point(451, 14);
            this.exportTypeLabel.Name = "exportTypeLabel";
            this.exportTypeLabel.Size = new System.Drawing.Size(113, 24);
            this.exportTypeLabel.TabIndex = 4;
            this.exportTypeLabel.Text = "Export Type";
            // 
            // exportFromDatePicker
            // 
            this.exportFromDatePicker.Enabled = false;
            this.exportFromDatePicker.Location = new System.Drawing.Point(455, 146);
            this.exportFromDatePicker.Name = "exportFromDatePicker";
            this.exportFromDatePicker.Size = new System.Drawing.Size(145, 20);
            this.exportFromDatePicker.TabIndex = 5;
            // 
            // exportButton
            // 
            this.exportButton.BackColor = System.Drawing.Color.LightBlue;
            this.exportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportButton.Location = new System.Drawing.Point(634, 206);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(141, 47);
            this.exportButton.TabIndex = 6;
            this.exportButton.Text = "Export";
            this.exportButton.UseVisualStyleBackColor = false;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromLabel.Location = new System.Drawing.Point(451, 109);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(55, 24);
            this.fromLabel.TabIndex = 7;
            this.fromLabel.Text = "From";
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toLabel.Location = new System.Drawing.Point(630, 109);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(33, 24);
            this.toLabel.TabIndex = 9;
            this.toLabel.Text = "To";
            // 
            // exportToDatePicker
            // 
            this.exportToDatePicker.Enabled = false;
            this.exportToDatePicker.Location = new System.Drawing.Point(634, 146);
            this.exportToDatePicker.Name = "exportToDatePicker";
            this.exportToDatePicker.Size = new System.Drawing.Size(141, 20);
            this.exportToDatePicker.TabIndex = 8;
            // 
            // clientNameLabel
            // 
            this.clientNameLabel.AutoSize = true;
            this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameLabel.Location = new System.Drawing.Point(14, 109);
            this.clientNameLabel.Name = "clientNameLabel";
            this.clientNameLabel.Size = new System.Drawing.Size(57, 24);
            this.clientNameLabel.TabIndex = 11;
            this.clientNameLabel.Text = "Client";
            // 
            // clientNameBox
            // 
            this.clientNameBox.Enabled = false;
            this.clientNameBox.FormattingEnabled = true;
            this.clientNameBox.Items.AddRange(new object[] {
            "Export All Data",
            "Export Data for this week (Monday to Sunday)",
            "Export Date Range (Based on Start Date)"});
            this.clientNameBox.Location = new System.Drawing.Point(18, 149);
            this.clientNameBox.Name = "clientNameBox";
            this.clientNameBox.Size = new System.Drawing.Size(320, 21);
            this.clientNameBox.TabIndex = 10;
            // 
            // matterNameLabel
            // 
            this.matterNameLabel.AutoSize = true;
            this.matterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterNameLabel.Location = new System.Drawing.Point(14, 190);
            this.matterNameLabel.Name = "matterNameLabel";
            this.matterNameLabel.Size = new System.Drawing.Size(61, 24);
            this.matterNameLabel.TabIndex = 13;
            this.matterNameLabel.Text = "Matter";
            // 
            // matterNameBox
            // 
            this.matterNameBox.Enabled = false;
            this.matterNameBox.FormattingEnabled = true;
            this.matterNameBox.Items.AddRange(new object[] {
            "Export All Data",
            "Export Data for this week (Monday to Sunday)",
            "Export Date Range (Based on Start Date)"});
            this.matterNameBox.Location = new System.Drawing.Point(18, 230);
            this.matterNameBox.Name = "matterNameBox";
            this.matterNameBox.Size = new System.Drawing.Size(320, 21);
            this.matterNameBox.TabIndex = 12;
            // 
            // ExportTimeCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 264);
            this.Controls.Add(this.matterNameLabel);
            this.Controls.Add(this.matterNameBox);
            this.Controls.Add(this.clientNameLabel);
            this.Controls.Add(this.clientNameBox);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.exportToDatePicker);
            this.Controls.Add(this.fromLabel);
            this.Controls.Add(this.exportButton);
            this.Controls.Add(this.exportFromDatePicker);
            this.Controls.Add(this.exportTypeLabel);
            this.Controls.Add(this.exportTypeBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.locationBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportTimeCardForm";
            this.Text = "Export Time Card";
            this.Load += new System.EventHandler(this.ExportTimeCardForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox locationBox;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.ComboBox exportTypeBox;
        private System.Windows.Forms.Label exportTypeLabel;
        private System.Windows.Forms.DateTimePicker exportFromDatePicker;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.DateTimePicker exportToDatePicker;
        private System.Windows.Forms.Label clientNameLabel;
        private System.Windows.Forms.ComboBox clientNameBox;
        private System.Windows.Forms.Label matterNameLabel;
        private System.Windows.Forms.ComboBox matterNameBox;
    }
}