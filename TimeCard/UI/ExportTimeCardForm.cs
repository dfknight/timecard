﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeCard.UI
{
    public partial class ExportTimeCardForm : Form
    {
        public ExportTimeCardForm()
        {
            InitializeComponent();
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            if(locationBox.Text == "")
            {
                MessageBox.Show("Save file location cannot be blank", "Error - Blank value", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (exportTypeBox.SelectedIndex < 0)
            {
                MessageBox.Show("Export type cannot be blank", "Error - Blank value", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(exportTypeBox.SelectedIndex == 0)
            {
                /*try
                {
                    // keep going
                    TimeCardForm.updateStatus("Export Completed");
                    this.Close();
                }
                catch
                {

                }*/
            }
            else if (exportTypeBox.SelectedIndex == 1)
            {
                //run sql query for this week
            }
            else if (exportTypeBox.SelectedIndex == 2)
            {
                //run sql query for date range
            }
            else if (exportTypeBox.SelectedIndex == 3)
            {
                //run sql query client
            }
            else if (exportTypeBox.SelectedIndex == 4)
            {
                //run sql query for matter
            }


        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                locationBox.Text = saveFileDialog1.FileName;
            }
        }

        private void exportTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(exportTypeBox.SelectedIndex == 2)
            {
                exportFromDatePicker.Enabled = true;
                exportToDatePicker.Enabled = true;
                clientNameBox.Enabled = false;
                matterNameBox.Enabled = false;
            }
            else if (exportTypeBox.SelectedIndex == 3)
            {
                exportFromDatePicker.Enabled = false;
                exportToDatePicker.Enabled = false;
                clientNameBox.Enabled = true;
                matterNameBox.Enabled = false;
            }
            else if (exportTypeBox.SelectedIndex == 4)
            {
                exportFromDatePicker.Enabled = false;
                exportToDatePicker.Enabled = false;
                clientNameBox.Enabled = false;
                matterNameBox.Enabled = true;
            }
            else
            {
                exportFromDatePicker.Enabled = false;
                exportToDatePicker.Enabled = false;
                clientNameBox.Enabled = false;
                matterNameBox.Enabled = false;
            }
        }

        private void ExportTimeCardForm_Load(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(clientNameBox, "ClientName", "clients");
            TimeCardForm.updateDropDown(matterNameBox, "MatterName", "matters");
        }
    }
}
