﻿namespace TimeCard.UI
{
    partial class LinkMatterAndClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkMatterAndClientForm));
            this.clientNameLabel = new System.Windows.Forms.Label();
            this.clientNameBox = new System.Windows.Forms.ComboBox();
            this.matterNameBox = new System.Windows.Forms.ComboBox();
            this.matterNameLabel = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // clientNameLabel
            // 
            this.clientNameLabel.AutoSize = true;
            this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameLabel.Location = new System.Drawing.Point(13, 13);
            this.clientNameLabel.Name = "clientNameLabel";
            this.clientNameLabel.Size = new System.Drawing.Size(129, 25);
            this.clientNameLabel.TabIndex = 0;
            this.clientNameLabel.Text = "Client Name";
            // 
            // clientNameBox
            // 
            this.clientNameBox.FormattingEnabled = true;
            this.clientNameBox.Location = new System.Drawing.Point(13, 42);
            this.clientNameBox.Name = "clientNameBox";
            this.clientNameBox.Size = new System.Drawing.Size(311, 21);
            this.clientNameBox.TabIndex = 1;
            // 
            // matterNameBox
            // 
            this.matterNameBox.FormattingEnabled = true;
            this.matterNameBox.Location = new System.Drawing.Point(13, 113);
            this.matterNameBox.Name = "matterNameBox";
            this.matterNameBox.Size = new System.Drawing.Size(311, 21);
            this.matterNameBox.TabIndex = 3;
            // 
            // matterNameLabel
            // 
            this.matterNameLabel.AutoSize = true;
            this.matterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterNameLabel.Location = new System.Drawing.Point(13, 84);
            this.matterNameLabel.Name = "matterNameLabel";
            this.matterNameLabel.Size = new System.Drawing.Size(135, 25);
            this.matterNameLabel.TabIndex = 2;
            this.matterNameLabel.Text = "Matter Name";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(249, 153);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // LinkMatterAndClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 187);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.matterNameBox);
            this.Controls.Add(this.matterNameLabel);
            this.Controls.Add(this.clientNameBox);
            this.Controls.Add(this.clientNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LinkMatterAndClientForm";
            this.RightToLeftLayout = true;
            this.Text = "Link Matter to Client";
            this.Load += new System.EventHandler(this.LinkMatterAndClientForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label clientNameLabel;
        private System.Windows.Forms.ComboBox clientNameBox;
        private System.Windows.Forms.ComboBox matterNameBox;
        private System.Windows.Forms.Label matterNameLabel;
        private System.Windows.Forms.Button saveButton;
    }
}