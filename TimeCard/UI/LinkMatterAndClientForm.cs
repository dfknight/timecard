﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace TimeCard.UI
{
    public partial class LinkMatterAndClientForm : Form
    {
        public LinkMatterAndClientForm()
        {
            InitializeComponent();
        }

        private void LinkMatterAndClientForm_Load(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(matterNameBox, "MatterName", "matters");
            TimeCardForm.updateDropDown(clientNameBox, "ClientName", "clients");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (matterNameBox.SelectedIndex < 0 || clientNameBox.SelectedIndex < 0)
            {
                MessageBox.Show("Matter Name and Client Name cannot be blank", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int clientId = 0;

                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();

                string sql = "SELECT ID FROM clients WHERE ClientName = '" + clientNameBox.GetItemText(clientNameBox.SelectedItem) + "'";

                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                using (SQLiteDataReader rd = command.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        clientId = Convert.ToInt16(rd["ID"]);
                    }
                }
                m_dbConnection.Close();
                m_dbConnection.Open();
                sql = "update matters set ClientID = '{0}' where MatterName = '{1}'";

                command = new SQLiteCommand(string.Format(sql, clientId, matterNameBox.SelectedItem.ToString()), m_dbConnection);
                command.ExecuteNonQuery();
                m_dbConnection.Close();
                TimeCardForm.updateStatus("Matter and Client link updated");
                this.Close();
            }
        }
    }
}
