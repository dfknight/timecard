﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using TimeCard.UI;

namespace TimeCard.UI
{
    public partial class NewClientForm : Form
    {
        public NewClientForm()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
             SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
             m_dbConnection.Open();


            string sql = "insert into clients (ClientName) values ('{0}')";
            try
            {
                SQLiteCommand command = new SQLiteCommand(string.Format(sql, clientNameBox.Text), m_dbConnection);
                command.ExecuteNonQuery();
                m_dbConnection.Close();
                TimeCardForm.updateStatus("New Client Added");
                this.Close();
            }
            catch(System.Data.SQLite.SQLiteException)
            {
                MessageBox.Show("The client name entered already exists in the database", "Error - Duplicate Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                m_dbConnection.Close();
            }
        }

        private void NewClientForm_Load(object sender, EventArgs e)
        {

        }
    }
}
