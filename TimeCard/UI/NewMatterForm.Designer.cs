﻿namespace TimeCard.UI
{
    partial class NewMatterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewMatterForm));
            this.MatterSaveButton = new System.Windows.Forms.Button();
            this.matterNameBox = new System.Windows.Forms.TextBox();
            this.matterNameLabel = new System.Windows.Forms.Label();
            this.matterCodeBox = new System.Windows.Forms.TextBox();
            this.matterBillCodeLabel = new System.Windows.Forms.Label();
            this.clientNameLabel = new System.Windows.Forms.Label();
            this.clientNameBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // MatterSaveButton
            // 
            this.MatterSaveButton.Location = new System.Drawing.Point(197, 236);
            this.MatterSaveButton.Name = "MatterSaveButton";
            this.MatterSaveButton.Size = new System.Drawing.Size(75, 23);
            this.MatterSaveButton.TabIndex = 5;
            this.MatterSaveButton.Text = "Save";
            this.MatterSaveButton.UseVisualStyleBackColor = true;
            this.MatterSaveButton.Click += new System.EventHandler(this.MatterSaveButton_Click);
            // 
            // matterNameBox
            // 
            this.matterNameBox.Location = new System.Drawing.Point(12, 119);
            this.matterNameBox.Name = "matterNameBox";
            this.matterNameBox.Size = new System.Drawing.Size(259, 20);
            this.matterNameBox.TabIndex = 4;
            // 
            // matterNameLabel
            // 
            this.matterNameLabel.AutoSize = true;
            this.matterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterNameLabel.Location = new System.Drawing.Point(12, 77);
            this.matterNameLabel.Name = "matterNameLabel";
            this.matterNameLabel.Size = new System.Drawing.Size(135, 25);
            this.matterNameLabel.TabIndex = 3;
            this.matterNameLabel.Text = "Matter Name";
            // 
            // matterCodeBox
            // 
            this.matterCodeBox.Location = new System.Drawing.Point(12, 199);
            this.matterCodeBox.Name = "matterCodeBox";
            this.matterCodeBox.Size = new System.Drawing.Size(259, 20);
            this.matterCodeBox.TabIndex = 7;
            // 
            // matterBillCodeLabel
            // 
            this.matterBillCodeLabel.AutoSize = true;
            this.matterBillCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterBillCodeLabel.Location = new System.Drawing.Point(12, 157);
            this.matterBillCodeLabel.Name = "matterBillCodeLabel";
            this.matterBillCodeLabel.Size = new System.Drawing.Size(127, 25);
            this.matterBillCodeLabel.TabIndex = 6;
            this.matterBillCodeLabel.Text = "Billing Code";
            // 
            // clientNameLabel
            // 
            this.clientNameLabel.AutoSize = true;
            this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameLabel.Location = new System.Drawing.Point(12, 9);
            this.clientNameLabel.Name = "clientNameLabel";
            this.clientNameLabel.Size = new System.Drawing.Size(129, 25);
            this.clientNameLabel.TabIndex = 8;
            this.clientNameLabel.Text = "Client Name";
            // 
            // clientNameBox
            // 
            this.clientNameBox.FormattingEnabled = true;
            this.clientNameBox.Location = new System.Drawing.Point(13, 37);
            this.clientNameBox.Name = "clientNameBox";
            this.clientNameBox.Size = new System.Drawing.Size(259, 21);
            this.clientNameBox.TabIndex = 9;
            // 
            // NewMatterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 268);
            this.Controls.Add(this.clientNameBox);
            this.Controls.Add(this.clientNameLabel);
            this.Controls.Add(this.matterCodeBox);
            this.Controls.Add(this.matterBillCodeLabel);
            this.Controls.Add(this.MatterSaveButton);
            this.Controls.Add(this.matterNameBox);
            this.Controls.Add(this.matterNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewMatterForm";
            this.Text = "Add New Matter";
            this.Load += new System.EventHandler(this.NewMatterForm_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button MatterSaveButton;
        public System.Windows.Forms.TextBox matterNameBox;
        public System.Windows.Forms.Label matterNameLabel;
        public System.Windows.Forms.TextBox matterCodeBox;
        public System.Windows.Forms.Label matterBillCodeLabel;
        public System.Windows.Forms.Label clientNameLabel;
        private System.Windows.Forms.ComboBox clientNameBox;
    }
}