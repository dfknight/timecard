﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using TimeCard.UI;

namespace TimeCard.UI
{
    public partial class NewMatterForm : Form
    {
        public NewMatterForm()
        {
            InitializeComponent();
        }

        private void MatterSaveButton_Click(object sender, EventArgs e)
        {
            int clientId = 0;

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            string sql = "SELECT ID FROM clients WHERE ClientName = '"+ clientNameBox.GetItemText(clientNameBox.SelectedItem) +"'";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    clientId = Convert.ToInt16(rd["ID"]);
                }
            }
            m_dbConnection.Close();

            m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            try
            {
                sql = "insert into matters (MatterName, MatterCode, ClientID) values ('{0}', '{1}', {2})";

                command = new SQLiteCommand(string.Format(sql, matterNameBox.Text, matterCodeBox.Text, clientId), m_dbConnection);
                command.ExecuteNonQuery();
                m_dbConnection.Close();
                TimeCardForm.updateStatus("New Matter Created");
                this.Close();
            }
            catch(System.Data.SQLite.SQLiteException)
            {
                MessageBox.Show("The matter name or code entered already exists in the database", "Error - Duplicate Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                m_dbConnection.Close();
            }

            
        }
        private void NewMatterForm_Load_1(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(clientNameBox, "ClientName", "clients");
        }


    }
}
