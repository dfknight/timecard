﻿namespace TimeCard.UI
{
    partial class RenameClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenameClientForm));
            this.origClientNameLabel = new System.Windows.Forms.Label();
            this.origClientNameBox = new System.Windows.Forms.ComboBox();
            this.newClientName = new System.Windows.Forms.Label();
            this.newClientNameText = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // origClientNameLabel
            // 
            this.origClientNameLabel.AutoSize = true;
            this.origClientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.origClientNameLabel.Location = new System.Drawing.Point(13, 13);
            this.origClientNameLabel.Name = "origClientNameLabel";
            this.origClientNameLabel.Size = new System.Drawing.Size(209, 25);
            this.origClientNameLabel.TabIndex = 0;
            this.origClientNameLabel.Text = "Original Client Name";
            // 
            // origClientNameBox
            // 
            this.origClientNameBox.FormattingEnabled = true;
            this.origClientNameBox.Location = new System.Drawing.Point(13, 42);
            this.origClientNameBox.Name = "origClientNameBox";
            this.origClientNameBox.Size = new System.Drawing.Size(290, 21);
            this.origClientNameBox.TabIndex = 1;
            // 
            // newClientName
            // 
            this.newClientName.AutoSize = true;
            this.newClientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newClientName.Location = new System.Drawing.Point(12, 97);
            this.newClientName.Name = "newClientName";
            this.newClientName.Size = new System.Drawing.Size(177, 25);
            this.newClientName.TabIndex = 2;
            this.newClientName.Text = "New Client Name";
            // 
            // newClientNameText
            // 
            this.newClientNameText.Location = new System.Drawing.Point(13, 125);
            this.newClientNameText.Name = "newClientNameText";
            this.newClientNameText.Size = new System.Drawing.Size(290, 20);
            this.newClientNameText.TabIndex = 3;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(241, 161);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // RenameClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 193);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.newClientNameText);
            this.Controls.Add(this.newClientName);
            this.Controls.Add(this.origClientNameBox);
            this.Controls.Add(this.origClientNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RenameClientForm";
            this.Text = "Rename Client Form";
            this.Load += new System.EventHandler(this.RenameClientForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label origClientNameLabel;
        private System.Windows.Forms.ComboBox origClientNameBox;
        private System.Windows.Forms.Label newClientName;
        private System.Windows.Forms.TextBox newClientNameText;
        private System.Windows.Forms.Button saveButton;
    }
}