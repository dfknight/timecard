﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeCard.UI
{
    public partial class RenameClientForm : Form
    {
        public RenameClientForm()
        {
            InitializeComponent();
        }

        private void RenameClientForm_Load(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(origClientNameBox, "ClientName", "clients");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (origClientNameBox.SelectedIndex < 0 || newClientNameText.Text == "")
            {
                MessageBox.Show("Original Client Name and New Client Name cannot be blank", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();
                try
                {
                    string sql = "update clients set ClientName = '{0}' where ClientName = '{1}'";

                    SQLiteCommand command = new SQLiteCommand(string.Format(sql, newClientNameText.Text, origClientNameBox.SelectedItem.ToString()), m_dbConnection);
                    command.ExecuteNonQuery();
                    m_dbConnection.Close();
                    TimeCardForm.updateStatus("Client Name updated");

                    this.Close();
                }
                catch (System.Data.SQLite.SQLiteException)
                {
                    MessageBox.Show("The client name already exists in the database", "Error - Duplicate Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    m_dbConnection.Close();
                }
            }
        }
    }
}
