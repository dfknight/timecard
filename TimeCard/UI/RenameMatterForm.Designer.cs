﻿namespace TimeCard.UI
{
    partial class RenameMatterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenameMatterForm));
            this.saveButton = new System.Windows.Forms.Button();
            this.newMatterNameText = new System.Windows.Forms.TextBox();
            this.newMatterName = new System.Windows.Forms.Label();
            this.origMatterNameBox = new System.Windows.Forms.ComboBox();
            this.origMatterNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(240, 157);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 9;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // newMatterNameText
            // 
            this.newMatterNameText.Location = new System.Drawing.Point(12, 121);
            this.newMatterNameText.Name = "newMatterNameText";
            this.newMatterNameText.Size = new System.Drawing.Size(290, 20);
            this.newMatterNameText.TabIndex = 8;
            // 
            // newMatterName
            // 
            this.newMatterName.AutoSize = true;
            this.newMatterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newMatterName.Location = new System.Drawing.Point(11, 93);
            this.newMatterName.Name = "newMatterName";
            this.newMatterName.Size = new System.Drawing.Size(183, 25);
            this.newMatterName.TabIndex = 7;
            this.newMatterName.Text = "New Matter Name";
            // 
            // origMatterNameBox
            // 
            this.origMatterNameBox.FormattingEnabled = true;
            this.origMatterNameBox.Location = new System.Drawing.Point(12, 38);
            this.origMatterNameBox.Name = "origMatterNameBox";
            this.origMatterNameBox.Size = new System.Drawing.Size(290, 21);
            this.origMatterNameBox.TabIndex = 6;
            // 
            // origMatterNameLabel
            // 
            this.origMatterNameLabel.AutoSize = true;
            this.origMatterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.origMatterNameLabel.Location = new System.Drawing.Point(12, 9);
            this.origMatterNameLabel.Name = "origMatterNameLabel";
            this.origMatterNameLabel.Size = new System.Drawing.Size(215, 25);
            this.origMatterNameLabel.TabIndex = 5;
            this.origMatterNameLabel.Text = "Original Matter Name";
            // 
            // RenameMatterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 188);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.newMatterNameText);
            this.Controls.Add(this.newMatterName);
            this.Controls.Add(this.origMatterNameBox);
            this.Controls.Add(this.origMatterNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RenameMatterForm";
            this.Text = "RenameMatterForm";
            this.Load += new System.EventHandler(this.RenameMatterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox newMatterNameText;
        private System.Windows.Forms.Label newMatterName;
        private System.Windows.Forms.ComboBox origMatterNameBox;
        private System.Windows.Forms.Label origMatterNameLabel;
    }
}