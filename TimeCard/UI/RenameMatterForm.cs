﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeCard.UI
{
    public partial class RenameMatterForm : Form
    {
        public RenameMatterForm()
        {
            InitializeComponent();
        }

        private void RenameMatterForm_Load(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(origMatterNameBox, "MatterName", "matters");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (origMatterNameBox.SelectedIndex < 0 || newMatterNameText.Text == "")
            {
                MessageBox.Show("Original Matter Name and New Matter Name cannot be blank", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();
                try
                {
                    string sql = "update matters set MatterName = '{0}' where MatterName = '{1}'";

                    SQLiteCommand command = new SQLiteCommand(string.Format(sql, newMatterNameText.Text, origMatterNameBox.SelectedItem.ToString()), m_dbConnection);
                    command.ExecuteNonQuery();
                    m_dbConnection.Close();
                    TimeCardForm.updateStatus("Matter Name updated");

                    this.Close();
                }
                catch (System.Data.SQLite.SQLiteException)
                {
                    MessageBox.Show("The Matter name already exists in the database", "Error - Duplicate Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    m_dbConnection.Close();
                }
            }
        }
    }
}
