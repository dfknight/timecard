﻿namespace TimeCard
{
    partial class TimeCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeCardForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMatterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateBillingCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joinMatterToClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.renameClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameMatterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.timeEntryTab = new System.Windows.Forms.TabPage();
            this.ManualEnterLabel = new System.Windows.Forms.Label();
            this.clientMatterTreeview = new System.Windows.Forms.TreeView();
            this.manualEnd = new System.Windows.Forms.DateTimePicker();
            this.manualStart = new System.Windows.Forms.DateTimePicker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.totalTimeTitleLabel = new System.Windows.Forms.Label();
            this.manualEnterButton = new System.Windows.Forms.Button();
            this.totalTimeLabel = new System.Windows.Forms.Label();
            this.endTimeLabel = new System.Windows.Forms.Label();
            this.startTimeLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.descriptionBox = new System.Windows.Forms.TextBox();
            this.timeViewTab = new System.Windows.Forms.TabPage();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.timeEntryTab.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.timeViewTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.exportToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(722, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newClientToolStripMenuItem,
            this.newMatterToolStripMenuItem,
            this.updateBillingCodeToolStripMenuItem,
            this.joinMatterToClientToolStripMenuItem,
            this.toolStripSeparator1,
            this.renameClientToolStripMenuItem,
            this.renameMatterToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(61, 24);
            this.exportToolStripMenuItem.Text = "Options";
            // 
            // newClientToolStripMenuItem
            // 
            this.newClientToolStripMenuItem.Name = "newClientToolStripMenuItem";
            this.newClientToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.newClientToolStripMenuItem.Text = "New Client";
            this.newClientToolStripMenuItem.Click += new System.EventHandler(this.newClientToolStripMenuItem_Click);
            // 
            // newMatterToolStripMenuItem
            // 
            this.newMatterToolStripMenuItem.Name = "newMatterToolStripMenuItem";
            this.newMatterToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.newMatterToolStripMenuItem.Text = "New Matter";
            this.newMatterToolStripMenuItem.Click += new System.EventHandler(this.newMatterToolStripMenuItem_Click);
            // 
            // updateBillingCodeToolStripMenuItem
            // 
            this.updateBillingCodeToolStripMenuItem.Name = "updateBillingCodeToolStripMenuItem";
            this.updateBillingCodeToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.updateBillingCodeToolStripMenuItem.Text = "Update Billing Code";
            this.updateBillingCodeToolStripMenuItem.Click += new System.EventHandler(this.updateBillingCodeToolStripMenuItem_Click);
            // 
            // joinMatterToClientToolStripMenuItem
            // 
            this.joinMatterToClientToolStripMenuItem.Name = "joinMatterToClientToolStripMenuItem";
            this.joinMatterToClientToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.joinMatterToClientToolStripMenuItem.Text = "Join Matter to Client";
            this.joinMatterToClientToolStripMenuItem.Click += new System.EventHandler(this.joinMatterToClientToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // renameClientToolStripMenuItem
            // 
            this.renameClientToolStripMenuItem.Name = "renameClientToolStripMenuItem";
            this.renameClientToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.renameClientToolStripMenuItem.Text = "Rename Client";
            this.renameClientToolStripMenuItem.Click += new System.EventHandler(this.deleteClientToolStripMenuItem_Click);
            // 
            // renameMatterToolStripMenuItem
            // 
            this.renameMatterToolStripMenuItem.Name = "renameMatterToolStripMenuItem";
            this.renameMatterToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.renameMatterToolStripMenuItem.Text = "Rename Matter";
            this.renameMatterToolStripMenuItem.Click += new System.EventHandler(this.renameMatterToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem1
            // 
            this.exportToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToCSVToolStripMenuItem});
            this.exportToolStripMenuItem1.Name = "exportToolStripMenuItem1";
            this.exportToolStripMenuItem1.Size = new System.Drawing.Size(52, 24);
            this.exportToolStripMenuItem1.Text = "Export";
            // 
            // exportToCSVToolStripMenuItem
            // 
            this.exportToCSVToolStripMenuItem.Name = "exportToCSVToolStripMenuItem";
            this.exportToCSVToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.exportToCSVToolStripMenuItem.Text = "Export to CSV";
            this.exportToCSVToolStripMenuItem.Click += new System.EventHandler(this.exportToCSVToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.timeEntryTab);
            this.tabControl1.Controls.Add(this.timeViewTab);
            this.tabControl1.Location = new System.Drawing.Point(0, 21);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(722, 521);
            this.tabControl1.TabIndex = 7;
            // 
            // timeEntryTab
            // 
            this.timeEntryTab.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeEntryTab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.timeEntryTab.Controls.Add(this.label2);
            this.timeEntryTab.Controls.Add(this.label1);
            this.timeEntryTab.Controls.Add(this.ManualEnterLabel);
            this.timeEntryTab.Controls.Add(this.clientMatterTreeview);
            this.timeEntryTab.Controls.Add(this.manualEnd);
            this.timeEntryTab.Controls.Add(this.manualStart);
            this.timeEntryTab.Controls.Add(this.statusStrip1);
            this.timeEntryTab.Controls.Add(this.totalTimeTitleLabel);
            this.timeEntryTab.Controls.Add(this.manualEnterButton);
            this.timeEntryTab.Controls.Add(this.totalTimeLabel);
            this.timeEntryTab.Controls.Add(this.endTimeLabel);
            this.timeEntryTab.Controls.Add(this.startTimeLabel);
            this.timeEntryTab.Controls.Add(this.startButton);
            this.timeEntryTab.Controls.Add(this.descriptionBox);
            this.timeEntryTab.Location = new System.Drawing.Point(4, 22);
            this.timeEntryTab.Margin = new System.Windows.Forms.Padding(0);
            this.timeEntryTab.Name = "timeEntryTab";
            this.timeEntryTab.Size = new System.Drawing.Size(714, 495);
            this.timeEntryTab.TabIndex = 0;
            this.timeEntryTab.Text = "Time Entry";
            // 
            // ManualEnterLabel
            // 
            this.ManualEnterLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ManualEnterLabel.AutoSize = true;
            this.ManualEnterLabel.Location = new System.Drawing.Point(244, 450);
            this.ManualEnterLabel.Name = "ManualEnterLabel";
            this.ManualEnterLabel.Size = new System.Drawing.Size(179, 13);
            this.ManualEnterLabel.TabIndex = 30;
            this.ManualEnterLabel.Text = "Enter Time Previously Not Recorded";
            // 
            // clientMatterTreeview
            // 
            this.clientMatterTreeview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clientMatterTreeview.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.clientMatterTreeview.HideSelection = false;
            this.clientMatterTreeview.Location = new System.Drawing.Point(0, 0);
            this.clientMatterTreeview.Name = "clientMatterTreeview";
            this.clientMatterTreeview.Size = new System.Drawing.Size(206, 468);
            this.clientMatterTreeview.TabIndex = 29;
            // 
            // manualEnd
            // 
            this.manualEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.manualEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.manualEnd.Location = new System.Drawing.Point(525, 446);
            this.manualEnd.Name = "manualEnd";
            this.manualEnd.Size = new System.Drawing.Size(82, 20);
            this.manualEnd.TabIndex = 28;
            // 
            // manualStart
            // 
            this.manualStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.manualStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.manualStart.Location = new System.Drawing.Point(435, 446);
            this.manualStart.Name = "manualStart";
            this.manualStart.Size = new System.Drawing.Size(84, 20);
            this.manualStart.TabIndex = 27;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.statusStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 471);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(712, 22);
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(58, 17);
            this.statusLabel.Text = "Welcome";
            // 
            // totalTimeTitleLabel
            // 
            this.totalTimeTitleLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.totalTimeTitleLabel.AutoSize = true;
            this.totalTimeTitleLabel.Location = new System.Drawing.Point(565, 39);
            this.totalTimeTitleLabel.Name = "totalTimeTitleLabel";
            this.totalTimeTitleLabel.Size = new System.Drawing.Size(57, 13);
            this.totalTimeTitleLabel.TabIndex = 25;
            this.totalTimeTitleLabel.Text = "Total Time";
            // 
            // manualEnterButton
            // 
            this.manualEnterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.manualEnterButton.Location = new System.Drawing.Point(613, 443);
            this.manualEnterButton.Name = "manualEnterButton";
            this.manualEnterButton.Size = new System.Drawing.Size(95, 25);
            this.manualEnterButton.TabIndex = 24;
            this.manualEnterButton.Text = "Manual Enter";
            this.manualEnterButton.UseVisualStyleBackColor = true;
            this.manualEnterButton.Click += new System.EventHandler(this.manualEnterButton_Click);
            // 
            // totalTimeLabel
            // 
            this.totalTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.totalTimeLabel.AutoSize = true;
            this.totalTimeLabel.Location = new System.Drawing.Point(628, 39);
            this.totalTimeLabel.Name = "totalTimeLabel";
            this.totalTimeLabel.Size = new System.Drawing.Size(43, 13);
            this.totalTimeLabel.TabIndex = 21;
            this.totalTimeLabel.Text = "0:00:00";
            // 
            // endTimeLabel
            // 
            this.endTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.endTimeLabel.AutoSize = true;
            this.endTimeLabel.Location = new System.Drawing.Point(492, 54);
            this.endTimeLabel.Name = "endTimeLabel";
            this.endTimeLabel.Size = new System.Drawing.Size(53, 13);
            this.endTimeLabel.TabIndex = 20;
            this.endTimeLabel.Text = "00:00 AM";
            // 
            // startTimeLabel
            // 
            this.startTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.startTimeLabel.AutoSize = true;
            this.startTimeLabel.Location = new System.Drawing.Point(492, 27);
            this.startTimeLabel.Name = "startTimeLabel";
            this.startTimeLabel.Size = new System.Drawing.Size(53, 13);
            this.startTimeLabel.TabIndex = 19;
            this.startTimeLabel.Text = "00:00 AM";
            // 
            // startButton
            // 
            this.startButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.startButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.startButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.startButton.FlatAppearance.BorderSize = 5;
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(247, 16);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(142, 59);
            this.startButton.TabIndex = 17;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = false;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // descriptionBox
            // 
            this.descriptionBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.descriptionBox.Location = new System.Drawing.Point(212, 91);
            this.descriptionBox.Multiline = true;
            this.descriptionBox.Name = "descriptionBox";
            this.descriptionBox.Size = new System.Drawing.Size(497, 349);
            this.descriptionBox.TabIndex = 16;
            this.descriptionBox.Text = "Description";
            // 
            // timeViewTab
            // 
            this.timeViewTab.Controls.Add(this.searchButton);
            this.timeViewTab.Controls.Add(this.searchTextBox);
            this.timeViewTab.Controls.Add(this.dataGridView1);
            this.timeViewTab.Location = new System.Drawing.Point(4, 22);
            this.timeViewTab.Margin = new System.Windows.Forms.Padding(0);
            this.timeViewTab.Name = "timeViewTab";
            this.timeViewTab.Size = new System.Drawing.Size(714, 495);
            this.timeViewTab.TabIndex = 1;
            this.timeViewTab.Text = "View Time Card";
            this.timeViewTab.UseVisualStyleBackColor = true;
            this.timeViewTab.Click += new System.EventHandler(this.timeViewTab_Click);
            // 
            // searchButton
            // 
            this.searchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.searchButton.FlatAppearance.BorderSize = 5;
            this.searchButton.Location = new System.Drawing.Point(623, 0);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(91, 21);
            this.searchButton.TabIndex = 2;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchTextBox.Location = new System.Drawing.Point(0, 0);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(625, 20);
            this.searchTextBox.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(-3, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(720, 475);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(425, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Start Time: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(428, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "End Time: ";
            // 
            // cancelToolStripMenuItem
            // 
            this.cancelToolStripMenuItem.Name = "cancelToolStripMenuItem";
            this.cancelToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.cancelToolStripMenuItem.Text = "Cancel Time Recording";
            this.cancelToolStripMenuItem.Click += new System.EventHandler(this.cancelToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(194, 6);
            // 
            // TimeCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(722, 538);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(722, 538);
            this.Name = "TimeCardForm";
            this.Text = "Time Card";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.timeEntryTab.ResumeLayout(false);
            this.timeEntryTab.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.timeViewTab.ResumeLayout(false);
            this.timeViewTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMatterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateBillingCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joinMatterToClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportToCSVToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage timeEntryTab;
        private System.Windows.Forms.TabPage timeViewTab;
        private System.Windows.Forms.TextBox descriptionBox;
        private System.Windows.Forms.Label totalTimeLabel;
        private System.Windows.Forms.Label endTimeLabel;
        private System.Windows.Forms.Label startTimeLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button manualEnterButton;
        private System.Windows.Forms.Label totalTimeTitleLabel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem renameClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameMatterToolStripMenuItem;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TextBox searchTextBox;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.DateTimePicker manualStart;
        private System.Windows.Forms.DateTimePicker manualEnd;
        private System.Windows.Forms.TreeView clientMatterTreeview;
        private System.Windows.Forms.Label ManualEnterLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem cancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}

