﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.SqlServer;
using System.Data.SQLite;
using TimeCard.UI;

namespace TimeCard
{
    public partial class TimeCardForm : Form
    {
        public static ToolStripStatusLabel statusLabel2;
        public static DateTime start;
        public static DateTime end;
        public static string timeWorked;
        public static string hoursWorked;
        public string matterID;
        public bool started = false;

        public static void updateStatus(string updateDescription)
        {
            statusLabel2.Text = updateDescription;
        }

        public static void updateTreeView(TreeView tree)
        {
            tree.Nodes.Clear();
            tree.BeginUpdate();

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            string sql = "SELECT ID, ClientName FROM clients";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    tree.Nodes.Add(Convert.ToString(rd["ID"]), Convert.ToString(rd["ClientName"]));
                    tree.Nodes[Convert.ToString(rd["ID"])].Tag = "Client";

                    string sql2 = "SELECT matters.ID, matters.MatterName " +
                                    "FROM matters " +
                                    "INNER JOIN clients on matters.ClientID = clients.ID " +
                                    "WHERE clients.ID = " + Convert.ToString(rd["ID"]);

                    SQLiteCommand command2 = new SQLiteCommand(sql2, m_dbConnection);
                    using (SQLiteDataReader rd2 = command2.ExecuteReader())
                    {
                        while (rd2.Read())
                        {
                            tree.Nodes[Convert.ToString(rd["ID"])].Nodes.Add(Convert.ToString(rd2["ID"]), Convert.ToString(rd2["MatterName"]));
                        }
                    }
                }
            }
            m_dbConnection.Close();
            tree.EndUpdate();
        }

        public static void updateDropDown(ComboBox dropDown, string field, string table)
        {
            dropDown.Items.Clear();

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            string sql = "SELECT " + field + " FROM " + table;

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    dropDown.Items.Add(Convert.ToString(rd[field]));
                }
            }
            m_dbConnection.Close();
        }

        private void InitialiseDb()
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();
            string sql = "CREATE TABLE IF NOT EXISTS clients (ID INTEGER PRIMARY KEY AUTOINCREMENT, ClientName varchar(75), unique(ClientName));";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "CREATE TABLE IF NOT EXISTS matters (ID INTEGER PRIMARY KEY AUTOINCREMENT, MatterName varchar(75), MatterCode varchar(15), ClientID integer, unique(MatterName), unique(MatterCode));";

            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "CREATE TABLE IF NOT EXISTS timings (MatterID integer, StartDateTime text, EndDateTime text, TimeWorked text, HoursWorked text, Description text);";

            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            m_dbConnection.Close();
        }

        public TimeCardForm()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
            statusLabel2 = statusLabel;
            startButton.BackColor = System.Drawing.Color.FromArgb(72, 209, 204);
            statusStrip1.SizingGrip = false;

            string runningLocation = System.IO.Path.GetDirectoryName(Application.ExecutablePath);

            if (!File.Exists(runningLocation + "\\timecard.sqlite"))
            {
                try
                {
                    SQLiteConnection.CreateFile("timecard.sqlite");
                    updateStatus("New Timecard database created");
                }
                catch
                {
                    MessageBox.Show("There was an error creating the SQLite database\n" +
                        "You may need to manually create it", "Error Creating SQL Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                try
                {

                    InitialiseDb();
                    updateStatus("New Timecard database created and initialised");
                }
                catch
                {
                    MessageBox.Show("There was an error when accessing the SQLite database\n" +
                        "Please make sure it exists in the same directory as this tool", "Error Accessing SQL Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                updateTreeView(clientMatterTreeview);
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewClientForm clientForm = new NewClientForm();
            using (clientForm)
            {
                clientForm.ShowDialog();
            }
            updateTreeView(clientMatterTreeview);

        }

        private void newMatterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewMatterForm matterForm = new NewMatterForm();
            using (matterForm)
            {
                matterForm.ShowDialog();
            }
            updateTreeView(clientMatterTreeview);

        }

        private void timeViewTab_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text == "View Time Card")
            {
                dataGridView1.Rows.Clear();
                dataGridView1.ColumnCount = 8;
                dataGridView1.Columns[0].Name = "Client";
                dataGridView1.Columns[1].Name = "Matter";
                dataGridView1.Columns[2].Name = "Billing Code";
                dataGridView1.Columns[3].Name = "Start";
                dataGridView1.Columns[4].Name = "Stop";
                dataGridView1.Columns[5].Name = "Time Worked";
                dataGridView1.Columns[6].Name = "Hours Worked";
                dataGridView1.Columns[7].Name = "Description";
                dataGridView1.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();

                // Check if there are any rows in the DB. 

                SQLiteCommand command = new SQLiteCommand("SELECT COUNT(*) FROM timings", m_dbConnection);

                bool rows = false;
                using (SQLiteDataReader read = command.ExecuteReader())
                {
                    if (read.Read()) {
                        int temp_n = 0;
                        try
                        {
                            if (int.TryParse(read.GetValue(read.GetOrdinal("rows")).ToString(), out temp_n) && temp_n > 0)
                            {
                                rows = true;
                            }
                        } catch (System.IndexOutOfRangeException ioore)
                        {
                            // No rows found in DB. Default value of false is maintained so do nothing.
                        }
                    }
                }

                if (rows)
                {
                    command = new SQLiteCommand("select clients.ClientName as Client, matters.MatterName as Matter, matters.MatterCode as \"Billing Code\", " +
                        "timings.StartDateTime as Start, timings.EndDateTime as Stop, timings.TimeWorked as \"Time Worked\", timings.HoursWorked as \"Hours Worked\", timings.Description " +
                        "From timings " +
                        "inner join matters on matters.ID = timings.MatterID " +
                        "inner join clients on clients.ID = matters.ClientID", m_dbConnection);

                    // SQLite Exception thrown if there are no rows in the DB
                    // This is handled with the check above.
                    using (SQLiteDataReader read = command.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            dataGridView1.Rows.Add(new object[]
                            {
                        read.GetValue(read.GetOrdinal("Client")),
                        read.GetValue(read.GetOrdinal("Matter")),
                        read.GetValue(read.GetOrdinal("Billing Code")),
                        read.GetValue(read.GetOrdinal("Start")),
                        read.GetValue(read.GetOrdinal("Stop")),
                        read.GetValue(read.GetOrdinal("Time Worked")),
                        read.GetValue(read.GetOrdinal("Hours Worked")),
                        read.GetValue(read.GetOrdinal("Description")),
                            });
                        }
                        dataGridView1.Update();
                        dataGridView1.Refresh();
                    }
                }
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {


        }

        private void updateBillingCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateBillingCodeForm billCode = new UpdateBillingCodeForm();

            using (billCode)
            {
                billCode.ShowDialog();
            }

        }

        private void joinMatterToClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LinkMatterAndClientForm linkMatter = new LinkMatterAndClientForm();

            using (linkMatter)
            {
                linkMatter.ShowDialog();
            }
        }

        private void deleteClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RenameClientForm renameClient = new RenameClientForm();

            using (renameClient)
            {
                renameClient.ShowDialog();
            }
            updateTreeView(clientMatterTreeview);
        }

        private void renameMatterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RenameMatterForm renameMatter = new RenameMatterForm();

            using (renameMatter)
            {
                renameMatter.ShowDialog();
            }
            updateTreeView(clientMatterTreeview);
        }

        private void exportToCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportTimeCardForm exportTimeCard = new ExportTimeCardForm();

            using (exportTimeCard)
            {
                exportTimeCard.ShowDialog();
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string searchQuery = searchTextBox.Text;
            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 8;
            dataGridView1.Columns[0].Name = "Client";
            dataGridView1.Columns[1].Name = "Matter";
            dataGridView1.Columns[2].Name = "Billing Code";
            dataGridView1.Columns[3].Name = "Start";
            dataGridView1.Columns[4].Name = "Stop";
            dataGridView1.Columns[5].Name = "Time Worked";
            dataGridView1.Columns[6].Name = "Hours Worked";
            dataGridView1.Columns[7].Name = "Description";
            dataGridView1.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();
            SQLiteCommand command = new SQLiteCommand();

            if (searchQuery == "")
            {
                command = new SQLiteCommand("select clients.ClientName as Client, matters.MatterName as Matter, matters.MatterCode as \"Billing Code\", " +
                "timings.StartDateTime as Start, timings.EndDateTime as Stop, timings.TimeWorked as \"Time Worked\", timings.HoursWorked as \"Hours Worked\", timings.Description " +
                "From timings " +
                "inner join matters on matters.ID = timings.MatterID " +
                "inner join clients on clients.ID = matters.ClientID", m_dbConnection);
            }
            else
            {
                command = new SQLiteCommand("select clients.ClientName as Client, matters.MatterName as Matter, matters.MatterCode as \"Billing Code\", " +
                "timings.StartDateTime as Start, timings.EndDateTime as Stop, timings.TimeWorked as \"Time Worked\", timings.HoursWorked as \"Hours Worked\", timings.Description " +
                "From timings " +
                "inner join matters on matters.ID = timings.MatterID " +
                "inner join clients on clients.ID = matters.ClientID " +
                "where timings.Description like '%" + searchQuery + "%'", m_dbConnection);
            }
            using (SQLiteDataReader read = command.ExecuteReader())
            {
                while (read.Read())
                {
                    dataGridView1.Rows.Add(new object[]
                    {
                        read.GetValue(read.GetOrdinal("Client")),
                        read.GetValue(read.GetOrdinal("Matter")),
                        read.GetValue(read.GetOrdinal("Billing Code")),
                        read.GetValue(read.GetOrdinal("Start")),
                        read.GetValue(read.GetOrdinal("Stop")),
                        read.GetValue(read.GetOrdinal("Time Worked")),
                        read.GetValue(read.GetOrdinal("Hours Worked")),
                        read.GetValue(read.GetOrdinal("Description")),
                    });
                }
                dataGridView1.Update();
                dataGridView1.Refresh();
            }
        }

        /*private void clientBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //matterBox.Items.Clear();

            int clientId = 0;

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            string sql = "SELECT ID FROM clients WHERE ClientName = '" + clientBox.Text + "'";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    clientId = Convert.ToInt16(rd["ID"]);
                }
            }
            m_dbConnection.Close();

            m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            sql = "SELECT MatterName FROM matters WHERE ClientId = " + clientId.ToString();

            command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    //matterBox.Items.Add(Convert.ToString(rd["MatterName"]));
                }
            }
            m_dbConnection.Close();
            //matterBox.SelectedIndex = 0;
        }*/

        /*private void matterBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string clientName = "";
            string matterCode = "";

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
            m_dbConnection.Open();

            string sql = "select clients.ClientName, matters.MatterCode, matters.ID " +
                            "from clients " +
                             "inner join matters on matters.ClientID = clients.ID " +
                             "where matters.MatterName = '" /*+ matterBox.Text + "'";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            using (SQLiteDataReader rd = command.ExecuteReader())
            {
                while (rd.Read())
                {
                    clientName = Convert.ToString(rd["ClientName"]);
                    matterCode = Convert.ToString(rd["MatterCode"]);
                    matterID = Convert.ToString(rd["ID"]);
                }
            }

            m_dbConnection.Close();
            //clientBox.SelectedIndex = clientBox.FindStringExact(clientName);
            //billingCodeLabel.Text = matterCode;


        }*/


        private void startButton_Click(object sender, EventArgs e)
        {
            if(started == false)
            {
                started = true;
                startButton.Text = "STOP";
                startButton.BackColor = System.Drawing.Color.FromArgb(255, 99, 71);
                endTimeLabel.Text = "00:00 AM";
                totalTimeLabel.Text = "0:00:00";
                start = DateTime.Now;
                startTimeLabel.Text = start.ToShortTimeString();
                statusLabel2.Text = "Time Recording Started";
            }
            else
            {
                if (clientMatterTreeview.SelectedNode == null || clientMatterTreeview.SelectedNode.Tag == "Client")
                {
                    MessageBox.Show("Please make sure that you have selected a matter, then click STOP", "Choose Matter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (descriptionBox.Text == "" || descriptionBox.Text == "Description")
                {
                    MessageBox.Show("Please make sure that you have enetered a description in the description box, then click STOP", "Write a description", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    started = false;
                    startButton.Text = "START";
                    startButton.BackColor = System.Drawing.Color.FromArgb(72, 209, 204);
                    end = DateTime.Now;
                    endTimeLabel.Text = end.ToShortTimeString();
                    timeWorked = end.Subtract(start).ToString();
                    hoursWorked = end.Subtract(start).TotalHours.ToString();
                    totalTimeLabel.Text = timeWorked.Substring(0, 8);

                    SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                    m_dbConnection.Open();

                    string sql = "SELECT ID FROM matters WHERE MatterName = '" + clientMatterTreeview.SelectedNode.Text + "'";

                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    using (SQLiteDataReader rd = command.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            matterID = Convert.ToString(rd["ID"]);
                        }
                    }
                    m_dbConnection.Close();

                    try
                    {
                        m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                        m_dbConnection.Open();

                        sql = "insert into timings(MatterID, StartDateTime, EndDateTime, TimeWorked, HoursWorked, Description) values('{0}','{1}','{2}','{3}', '{4}', '{5}')";

                        command = new SQLiteCommand(string.Format(sql, matterID, start.ToString(), end.ToString(), timeWorked, hoursWorked, descriptionBox.Text), m_dbConnection);
                        command.ExecuteNonQuery();
                        m_dbConnection.Close();
                        updateStatus("Time Record Saved");
                    }
                    catch
                    {
                        MessageBox.Show("There was an issue saving your work, please try again", "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    descriptionBox.Text = "Description";
                }
            }
            
        }

        private void manualEnterButton_Click(object sender, EventArgs e)
        {
            if (clientMatterTreeview.SelectedNode == null || clientMatterTreeview.SelectedNode.Tag == "Client")
            {
                MessageBox.Show("Please make sure that you have selected a client and a matter, then try again", "Choose Matter and CLient", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (descriptionBox.Text == "" || descriptionBox.Text == "Description")
            {
                MessageBox.Show("Please make sure that you have enetered a description in the description box, then try again", "Write a description", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                start = manualStart.Value;
                end = manualEnd.Value;
                timeWorked = end.Subtract(start).ToString();
                hoursWorked = end.Subtract(start).TotalHours.ToString();
                totalTimeLabel.Text = timeWorked.Substring(0, 8);

                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();

                string sql = "SELECT ID FROM matters WHERE MatterName = '" + clientMatterTreeview.SelectedNode.Text + "'";

                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                using (SQLiteDataReader rd = command.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        matterID = Convert.ToString(rd["ID"]);
                    }
                }
                m_dbConnection.Close();

                try
                {
                    m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                    m_dbConnection.Open();

                    sql = "insert into timings(MatterID, StartDateTime, EndDateTime, TimeWorked, HoursWorked, Description) values('{0}','{1}','{2}','{3}', '{4}', '{5}')";

                    command = new SQLiteCommand(string.Format(sql, matterID, start.ToString(), end.ToString(), timeWorked, hoursWorked, descriptionBox.Text), m_dbConnection);
                    command.ExecuteNonQuery();
                    m_dbConnection.Close();
                    updateStatus("Time Record Saved");
                    descriptionBox.Text = "Description";
                }
                catch
                {
                    MessageBox.Show("There was an issue saving your work, please try again", "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void cancelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (started == true)
            {
                started = false;
                startButton.Text = "START";
                startButton.BackColor = System.Drawing.Color.FromArgb(72, 209, 204);
                startTimeLabel.Text = "00:00 AM";
                statusLabel.Text = "Time Recording Cancelled";
            }
        }
    }
}
