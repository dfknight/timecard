﻿namespace TimeCard.UI
{
    partial class UpdateBillingCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateBillingCodeForm));
            this.matterNameLabel = new System.Windows.Forms.Label();
            this.matterNameBox = new System.Windows.Forms.ComboBox();
            this.billCodeLabel = new System.Windows.Forms.Label();
            this.billCodeBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // matterNameLabel
            // 
            this.matterNameLabel.AutoSize = true;
            this.matterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matterNameLabel.Location = new System.Drawing.Point(13, 13);
            this.matterNameLabel.Name = "matterNameLabel";
            this.matterNameLabel.Size = new System.Drawing.Size(135, 25);
            this.matterNameLabel.TabIndex = 0;
            this.matterNameLabel.Text = "Matter Name";
            // 
            // matterNameBox
            // 
            this.matterNameBox.FormattingEnabled = true;
            this.matterNameBox.Location = new System.Drawing.Point(18, 41);
            this.matterNameBox.Name = "matterNameBox";
            this.matterNameBox.Size = new System.Drawing.Size(331, 21);
            this.matterNameBox.TabIndex = 1;
            // 
            // billCodeLabel
            // 
            this.billCodeLabel.AutoSize = true;
            this.billCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billCodeLabel.Location = new System.Drawing.Point(13, 87);
            this.billCodeLabel.Name = "billCodeLabel";
            this.billCodeLabel.Size = new System.Drawing.Size(256, 25);
            this.billCodeLabel.TabIndex = 2;
            this.billCodeLabel.Text = "New Billing Code Number";
            // 
            // billCodeBox
            // 
            this.billCodeBox.Location = new System.Drawing.Point(18, 116);
            this.billCodeBox.Name = "billCodeBox";
            this.billCodeBox.Size = new System.Drawing.Size(331, 20);
            this.billCodeBox.TabIndex = 3;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(333, 155);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // UpdateBillingCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 189);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.billCodeBox);
            this.Controls.Add(this.billCodeLabel);
            this.Controls.Add(this.matterNameBox);
            this.Controls.Add(this.matterNameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateBillingCodeForm";
            this.Text = "Update Billing Code";
            this.Load += new System.EventHandler(this.UpdateBillingCodeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label matterNameLabel;
        private System.Windows.Forms.ComboBox matterNameBox;
        private System.Windows.Forms.Label billCodeLabel;
        private System.Windows.Forms.TextBox billCodeBox;
        private System.Windows.Forms.Button saveButton;
    }
}