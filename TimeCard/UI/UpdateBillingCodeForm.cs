﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace TimeCard.UI
{
    public partial class UpdateBillingCodeForm : Form
    {
        public UpdateBillingCodeForm()
        {
            InitializeComponent();
        }

        private void UpdateBillingCodeForm_Load(object sender, EventArgs e)
        {
            TimeCardForm.updateDropDown(matterNameBox, "MatterName", "matters");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (matterNameBox.SelectedIndex < 0 || billCodeBox.Text == "")
            {
                MessageBox.Show("Matter Name and Billing Code cannot be blank", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=timecard.sqlite;");
                m_dbConnection.Open();
                try
                {
                    string sql = "update matters set MatterCode = '{0}' where MatterName = '{1}'";

                    SQLiteCommand command = new SQLiteCommand(string.Format(sql, billCodeBox.Text, matterNameBox.SelectedItem.ToString()), m_dbConnection);
                    command.ExecuteNonQuery();
                    m_dbConnection.Close();
                    TimeCardForm.updateStatus("Billing Code updated");

                    this.Close();
                }
                catch (System.Data.SQLite.SQLiteException)
                {
                    MessageBox.Show("The billing code already exists in the database", "Error - Duplicate Value", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    m_dbConnection.Close();
                }
            }
        }
    }
}
